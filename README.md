# Password manager API


## Installation

Install [Node.js](https://nodejs.org/en/), [Git](https://git-scm.com/) and then:

```sh
git clone https://gitlab.com/thatshailesh/pwd-manager.git
cd pwd-manager
npm install
npm start
```


## Running the tests

```
$ npm test
```

# API Validation
 
 By using celebrate the req.body schema becomes defined at route level, so even frontend devs can read what an API endpoint expects without need to writing a documentation that can get outdated quickly.

 ```js
 route.post('/signup', 
  celebrate({
    body: Joi.object({
      username: Joi.string().required(),
      password: Joi.string().required(),
    }),
  }),
  controller.signup)
 ```

 **Example error**

 ```json
 {
  "errors": {
    "message": "child \"email\" fails because [\"email\" is required]"
  }
 } 
 ```

## User Register
- API

 - POST `/api/auth/signup`
    ```
    {
     username: string;
     password: string;
    }
    ```

## User Login
- API

 - POST `/api/auth/login`
    ```
    {
     username: string;
     password: string;
    }
    ```

## Password API

### Get One
- API

 - GET `/api/password/:id`
 - Result
   ```
    {
      passwordData: {
          _id: string,
          accountName: string,
          username: string,
          password: string,
          userId: string
      }
    }
   ```

### Create
- API

 - POST `/api/password`
 - Body
   ```
    {
      accountName?: string,
      username?: string,
      password?: string,
    }
   ```

### Update
- API

 - POST `/api/password/:id`
 - Body
   ```
    {
      accountName?: string,
      username?: string,
      password?: string,
    }
   ```
### Delete

- API

 - DELETE `/api/password/:id`
 - RESPONSE
   ```
    {
      deleted: boolean
    }
   ```

### Get One
- API

 - GET ONE `/api/password/:id`
 - RESPONSE
   ```
    {
      passwordData: {
          accountName: string,
          username: string,
          userId: string
      }
    }
   ```
### Get ALL
- API

 - GET `/api/password`
 - RESPONSE
   ```
    {
      passwordData: [{
          accountName: string,
          username: string,
          userId: string
      }]
    }
   ```
  
### Soft Delete
- API

 - POST `/api/password/:id`
 - Body
    ```
    {
        isRemoved: true
    }
   ```
  
##### Time span after which document will be deleted is configurable



## Folder Structure
```
src
│   app.ts          # App entry point
└───api             # Express route controllers for all the endpoints of the app
└───config          # Environment variables and configuration related stuff
└───jobs            # Jobs definitions for agenda.js
└───loaders         # Split the startup process into modules
└───models          # Database models
└───services        # All the business logic is here
└───types           # Type declaration files (d.ts) for Typescript
```

## Author

- **Shailesh Shekhawat** - _Initial work_ - [Github](https://gitlab.com/thatshailesh/pwd-manager.git)

## License

This project is licensed under the MIT License
