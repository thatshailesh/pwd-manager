
import { Chance } from 'chance';
const chance = new Chance();

export const passwordData: any = {
    accountName: chance.name(),
    username: chance.email(),
    password: chance.string(),
};


export function generatePasswordData(total, userId): any {
    const passwordDataList = [];

    for (let i = 0; i < total; i++) 
        passwordDataList.push({
            _id: chance.guid(),
            userId,
            accountName: chance.name(),
            username: chance.email(),
            password: chance.string(),
        });
    return passwordDataList;
}

