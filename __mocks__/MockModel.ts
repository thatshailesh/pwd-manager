// import { User } from '../src/interfaces/User';
import Chance from 'chance';
const chance = new Chance();
import _ from 'underscore';
const MODEL_METHODS = [
    'create',
    'findOne',
    'exists',
    'deleteOne',
    'findOneAndUpdate',
];

export default class MockModel {
    dbRows: any;
    queryResult: any;
    sort;
    constructor() {
        this.dbRows = [];
        this.queryResult = null;

        for (const method of MODEL_METHODS) 
            this[method as string] = jest.fn((query, data) => {
                if (method === 'findOne' || method === 'exists') 
                    this.queryResult = _.findWhere(this.dbRows, query);

                if (this.queryResult === undefined) 
                    this.queryResult = null;
                
                if (method === 'create') {
                    query._id = chance.guid();
                    this.dbRows.push(query);
                    this.queryResult = query;
                }
                
                if (method === 'findOneAndUpdate') {
                    const index = _.findIndex(this.dbRows, query);
                    this.dbRows[index] = data;
                    this.queryResult = data;
                }

                if (this.queryResult) 
                    this.queryResult = Object.assign(this.queryResult, { toObject: this._toObject.bind(this) });

                return Promise.resolve(this.queryResult);
            }) as any;

        this.find = jest.fn(function (query) {
            this.queryResult = {
                data: _.where(this.dbRows, query),
            };

            this.queryResult = Object.assign(
                this.queryResult, {
                    sort: this._sort.bind(this), 
                    limit: this._limit.bind(this), 
                    skip: this._skip.bind(this),
                }
            );
            return this.queryResult;
        });
        
    }

    _setMockData(results: any) {
        this.dbRows = results;
    }

    _clearMockData(): any {
        this.dbRows = [];
    }

    _toObject(): any {
        console.log('coming here in toobject');
        if (this.queryResult) {
            if (this.queryResult.salt) 
                delete this.queryResult.salt;
            
            return this.queryResult;
        }
        return {};
    }

    _sort(order) {
        if (this.queryResult && Array.isArray(this.queryResult)) {
            if (order._id === 1) 
                return _.sortBy(this.queryResult.data, function(o) {
                    o._id;
                });
            return _.sortBy(this.queryResult.data, function(o) {
                -o._id;
            });
        } 
        return this.queryResult;
    }
    _skip(number) {
        const datatAfterSkip = this.queryResult.data;

        this.queryResult.data = _.rest(datatAfterSkip, number);
        return this.queryResult;
    }
    _limit(total) {
        if (this.queryResult && this.queryResult.data) 
            return _.first(this.queryResult.data, total);

        return this.queryResult;
    }
}

// const chance = new Chance();
// let savedUser: User;

// const modifyResponse = (obj: User) => ({
//     ...obj,
//     toObject: () => ({ ...obj }),
// });

// export default {
//     create: (user: User) => {
//         const userClone = { ...user };
//         userClone._id = chance.guid();
//         savedUser = userClone;
//         return modifyResponse(userClone);
//     },
//     findOne: () => modifyResponse(savedUser),
//     exists: () => true,
// };