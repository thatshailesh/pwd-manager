import 'reflect-metadata';
import AuthService from '../src/services/AuthService';
import { AuthService as IAuthService } from '../src/interfaces/AuthService';
import { User as IUser } from '../src/interfaces/User';
import LoggerInstance from '../src/loaders/logger';
import { testUser as userInput } from '../__fixtures__/user';
import userMockModel from '../__mocks__/MockModel';

jest.mock('../src/models/User', () => new userMockModel());

describe('AuthService', () => {
    let token: string;
    let authServiceInstance: IAuthService;
    let userModelInstance;
    beforeEach(() => {
        jest.clearAllMocks();  //reset mock calls
        authServiceInstance = new AuthService();
        userModelInstance = new userMockModel(); 
        (authServiceInstance as any).userModel = userModelInstance;
        (authServiceInstance as any).logger = LoggerInstance;
    });

    it('should be able to create generate token', () => {
        token = authServiceInstance.generateToken(userInput);
        expect(token).not.toBeNull();
        expect(typeof token).toBe('string');
    });

    it('should be able to verify valid token', () => {
        const result = authServiceInstance.verifyToken(token) as IUser;
        expect(result).not.toBeNull();
        expect(result.username).toEqual(userInput.username);
        expect(result.password).toEqual(userInput.password);
    });

    it('should fail to verify with invalid token', () => {
        const result = () => authServiceInstance.verifyToken('qwrqwrq') as IUser;
        expect(result).toThrowError('jwt malformed');
    });

    it('should be able to signup user', async () => {
        const result = await authServiceInstance.signUp(userInput);
        expect(result).not.toBeNull();
        expect(typeof result).toBe('object');
        expect(result).toHaveProperty('user');
        expect(result.user.username).toBe(userInput.username);
    });

    it('should be able to login with valid credentials', async () => {
        const signUp = await authServiceInstance.signUp(userInput);
        userModelInstance._setMockData([
            signUp.user,
        ]);
        const result = await authServiceInstance.login(userInput.username, userInput.password);
        expect(result).not.toBeNull();
        expect(result).toHaveProperty('user');
        expect(result.user).toHaveProperty('username');
        expect(result.user.username).toEqual(userInput.username);
    });

    it('should fail to login with wrong credentials', async () => {
        const signUp = await authServiceInstance.signUp(userInput);
        userModelInstance._setMockData([
            signUp.user,
        ]);
        await expect(authServiceInstance
            .login(userInput.username, 'correctpass'))
            .rejects.toThrow('Invalid Credentials');
    });
});
