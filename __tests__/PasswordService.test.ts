import 'reflect-metadata';
import { PasswordService as IPasswordService } from '../src/interfaces/PasswordService';
import PasswordService from '../src/services/PasswordService';
import LoggerInstance from '../src/loaders/logger';
import passwordMockModel from '../__mocks__/MockModel';
import { Chance } from 'chance';
import { passwordData, generatePasswordData } from '../__fixtures__/passwordData';
import { PasswordData } from '../src/interfaces/PasswordData';\

jest.mock('../src/models/Password', () => new passwordMockModel());

describe('PasswordService', () => {
    let passwordServiceInstance: IPasswordService;
    let passwordModelInstance;
    const chance = new Chance();
    beforeEach(() => {
        jest.clearAllMocks();  //reset mock calls
        passwordServiceInstance = new PasswordService();
        passwordModelInstance = new passwordMockModel(); 
        (passwordServiceInstance as any).passwordModel = passwordModelInstance;
        (passwordServiceInstance as any).logger = LoggerInstance;
    });
    it('should be able to create password data', async () => {
        passwordData.userId = chance.guid();

        const result = await passwordServiceInstance.createData(passwordData);
        expect(result).not.toBeNull();
        expect(result).toHaveProperty('passwordData');
        expect(result.passwordData.accountName).toEqual(passwordData.accountName);
    });

    it('should be able to update the password', async () => {
        const mockPassData = { ...passwordData };
        mockPassData._id = chance.guid();
        mockPassData.userId = chance.guid();
        passwordModelInstance._setMockData([
            mockPassData,
        ]);

        const dataToUpdate = { username: chance.email(), userId: mockPassData.userId };

        const result = await passwordServiceInstance
            .updateOne(mockPassData._id,  dataToUpdate as PasswordData);

        expect(result).not.toBeNull();
        expect(result).toHaveProperty('passwordData');
        expect(result.passwordData.username).toEqual(dataToUpdate.username);
    });

    it('should be able to get one password data', async () => {
        const mockPassData = { ...passwordData };
        mockPassData._id = chance.guid();
        passwordModelInstance._setMockData([
            mockPassData,
        ]);
        const result = await passwordServiceInstance.findOne(mockPassData._id);
        expect(result).not.toBeNull();
        expect(result).toHaveProperty('passwordData');
        expect(result.passwordData.username).toEqual(mockPassData.username);
    });

    it('should be able to get paginated password data for the user', async () => {
        const userId = chance.guid();
        const passwordDataList = generatePasswordData(10, userId);
        passwordModelInstance._setMockData(passwordDataList);
        const paging = {
            recordsPerPage: 5,
            pageNumber: 0,
            sort: 'descending',
        };
        const result = await passwordServiceInstance.findAll(userId, paging);
        expect(result).not.toBeNull();
        expect(result).toHaveProperty('pageNumber');
        expect(result).toHaveProperty('recordsPerPage');
        expect(result).toHaveProperty('data');
        expect(result.data.length).toBe(5);
    });
});