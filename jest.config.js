module.exports = {
    roots: ['<rootDir>'],
    transform: {
        '^.+\\.ts?$': 'ts-jest',
    },
    testRegex: '(test|spec)\\.ts?$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    preset: 'ts-jest',
    globals: {
        'ts-jest': {
            diagnostics: false,
        },
    },
    // collectCoverageFrom: ['**/*.ts'],
    modulePathIgnorePatterns: [
        'src/app.ts',
        'src/types',
        'src/loaders',
    ],
    testEnvironment: 'node',
    testPathIgnorePatterns: [
        './src/loaders',
        './src/app.ts',
        './src/models',
    ],
    collectCoverageFrom: [
        'src/services/**',
    ],
};
