import express from 'express';
import password from './routes/password';
import auth from './routes/auth';
const router = express.Router();

router.use('/password', password);
router.use('/auth', auth);

export default router;