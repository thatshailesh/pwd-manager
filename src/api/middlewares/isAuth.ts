import { Unauthorized } from '@curveball/http-errors';
import { Response, NextFunction, Request } from 'express';
import { Container } from 'typedi';
import AuthService from '../../services/AuthService';
import mongoose from 'mongoose';
import { User as IUser } from '../../interfaces/User';

export default async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    // check header or url parameters or post parameters for token
    let token = req.body.token || req.params.token || req.headers['x-access-token'];

    if (!token && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') 
        token = req.headers.authorization.split(' ')[1];
    else if (!token && req.query && req.query.token) 
        token = req.query.token;


    //decode token
    if (token) 
        try {
            const authServiceInstance = Container.get(AuthService);
            const jwtResponse = authServiceInstance.verifyToken(token);
            console.log('jwtResponse', jwtResponse);
            const userModel = Container.get('userModel') as mongoose.Model<IUser & mongoose.Document>;

            const user = await userModel.findById(jwtResponse._id);
            console.log('user', user);

            if (!user) return next(new Unauthorized('Unauthorized'));

            const currentUser = user.toObject();

            req.currentUser = currentUser;
            return next();
        } catch (err) {
            return next(new Unauthorized('Unauthorized'));
        }
    return next(new Unauthorized('Unauthorized'));
};
