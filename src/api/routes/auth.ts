import express from 'express';
import { Container } from 'typedi';
import winston from 'winston';
import AuthService from '../../services/AuthService';
import { User } from '../../interfaces/User';
import { celebrate, Joi, errors } from 'celebrate';


const router = express.Router();

router.post('/signup',
    celebrate({
        body: Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
            name: Joi.string().required(),
        }),
    }), 
    async (req, res, next) => {
        const logger: winston.Logger = Container.get('logger');
        logger.debug('Calling Sign-Up endpoint with body: %o', req.body );
        try {
            const authServiceInstance = Container.get(AuthService);
            const result = await authServiceInstance.signUp(req.body as User);
            return res.status(201).json(result);
        } catch (err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    });

router.post('/login',
    celebrate({
        body: Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }), 
    async (req, res, next) => {
        const logger: winston.Logger = Container.get('logger');
        logger.debug('Calling login endpoint with body: %o', req.body );

        try {
            const authServiceInstance = Container.get(AuthService);
            const { username, password } = req.body;
            const result = await authServiceInstance.login(username, password);
            return res.status(200).json(result);
        } catch (e) {
            logger.error('🔥 error: %o', e);
            return next(e);
        }
    });
router.use(errors());

export default router;