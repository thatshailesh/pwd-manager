import express, { Request, Response, NextFunction } from 'express';
import isAuth from '../middlewares/isAuth';
import { Container } from 'typedi';
import winston from 'winston';
import { PasswordService as IPasswordService } from '../../interfaces/PasswordService';
import PasswordService from '../../services/PasswordService';
import { PaginOptions } from '../../interfaces/PagingOptions';
import { celebrate, Joi, errors } from 'celebrate';

const router = express.Router();

router.use(isAuth);

router.post('/:id', 
    celebrate({
        body: Joi.object({
            username: Joi.string().optional(),
            password: Joi.string().optional(),
            accountName: Joi.string().optional(),
            isRemoved: Joi.boolean().optional(),
        }),
        params: Joi.object({
            id: Joi.string().required(),
        }),
    }), 
    async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');

        logger.debug('Calling update password data endpoint with body: %o', req.body );
        try {
            const passwordServiceInstance = Container.get(PasswordService) as IPasswordService;
            const { id } = req.params;

            const data = { ...req.body };
            data.userId = req.currentUser._id;

            const result = await passwordServiceInstance.updateOne(id, data);
            return res.status(200).json(result);

        }catch(err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    });

router.delete('/:id', 
    celebrate({
        params: Joi.object({
            id: Joi.string().required(),
        }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');

        logger.debug('Calling delete password data endpoint with body: %o', req.params.id );
        try {
            const passwordServiceInstance = Container.get(PasswordService) as IPasswordService;
            const { id } = req.params;

            const result = await passwordServiceInstance.deleteOne(id);
            return res.status(200).json(result);

        }catch(err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    });

router.post('/',
    celebrate({
        body: Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
            accountName: Joi.string().required(),
        }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
        const logger: winston.Logger = Container.get('logger');
        logger.debug('Calling create password data endpoint with body: %o', req.body );
        try {
            const passwordServiceInstance = Container.get(PasswordService) as PasswordService;
            const data = { ...req.body };
            data.userId = req.currentUser._id;

            const result = await passwordServiceInstance.createData(data);
            return res.status(201).json(result);
        }catch(err) {
            logger.error('🔥 error: %o', err);
            return next(err);
        }
    });

router.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
    const logger: winston.Logger = Container.get('logger');

    logger.debug('Calling get one password data endpoint with id: %o', req.params.id );
    try {
        const passwordServiceInstance = Container.get(PasswordService) as PasswordService;
        const { id } = req.params;

        const result = await passwordServiceInstance.findOne(id);
        return res.status(200).json(result);

    }catch(err) {
        logger.error('🔥 error: %o', err);
        return next(err);
    }
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
    const logger: winston.Logger = Container.get('logger');

    logger.debug('Calling get all password data endpoint: with id %o', req.params.id );

    try {
        const passwordServiceInstance = Container.get(PasswordService) as PasswordService;
        const { _id } = req.currentUser;
        const { pageNumber = 0, recordsPerPage = 10, sort = 'descending', criteria = null } = req.query;
        const pagingOption: PaginOptions = {
            pageNumber: parseInt(pageNumber, 10), 
            recordsPerPage: parseInt(recordsPerPage, 10),
            sort,
        };
        if (criteria) 
            pagingOption.criteria = criteria;
        
        const result = await passwordServiceInstance.findAll(_id, pagingOption);
        return res.status(200).json(result);

    }catch(err) {
        logger.error('🔥 error: %o', err);
        return next(err);
    }
});

router.use(errors());

export default router;