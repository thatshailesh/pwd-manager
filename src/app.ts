import 'reflect-metadata'; // We need this in order to use @Decorators
import express from 'express';
import Logger from './loaders/logger';
import config from 'config';

async function startServer(): Promise<void> {
    const app = express();

    await require('./loaders').default(app);

    const port = config.get('port');
    app.listen(port, () => {
        Logger.info(`
          ################################################
          🛡️  Server listening on port: ${port} 🛡️ 
          ################################################
        `);
    });
}

startServer();