
import { User } from './User';
export interface AuthService {
    generateToken(user: User): string;
    verifyToken(token: string): any;
    signUp(user: User): Promise<{ user: User }>;
    login(username: string, password: string): Promise<{user: User; token: string}>;
}