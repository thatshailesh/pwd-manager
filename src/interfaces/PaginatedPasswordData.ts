import { PasswordData } from './PasswordData';

export interface PaginatedPasswordData {
    recordsPerPage: number;
    pageNumber: number;
    data: PasswordData[];
}