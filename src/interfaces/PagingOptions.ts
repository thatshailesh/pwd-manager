
export interface PaginOptions {
    recordsPerPage: number;
    pageNumber: number;
    sort?: string;
    criteria?: string;
}