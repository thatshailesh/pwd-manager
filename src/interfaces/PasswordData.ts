export interface PasswordData {
    username: string;
    password: string;
    accountName: string;
    userId: string;
    _id?: string;
    isRemoved?: boolean;
}