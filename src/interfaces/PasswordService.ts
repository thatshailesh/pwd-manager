
import { PasswordData } from './PasswordData';
import { PaginOptions } from './PagingOptions';
import { PaginatedPasswordData } from './PaginatedPasswordData';
export interface PasswordService {
    createData(data: PasswordData): Promise<{passwordData: PasswordData}>;
    updateOne(id: string, data: PasswordData): Promise<{passwordData: PasswordData}>;
    deleteOne(id: string): Promise<{deleted: boolean}>;
    findOne(id: string): Promise<{passwordData: PasswordData}>;
    findAll(userId: string, paging: PaginOptions): Promise<PaginatedPasswordData>;
}