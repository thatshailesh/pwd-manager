import { Container } from 'typedi';
import winston = require('winston');
import PasswordService from '../services/PasswordService';

export default class DeleteRecordJob {
    public async handler(job: any, done: any): Promise<void> {
        const Logger = Container.get('logger') as winston.Logger;
        try {
            Logger.debug('✌️ Deletion Job triggered!');
            const { _id } = job.attrs.data; 
            const passwordServiceInstance = Container.get(PasswordService);
            await passwordServiceInstance.deleteOne(_id);
            done();
        } catch (e) {
            Logger.error('🔥 Error with delete Job: %o', e);
            done(e);
        }
    }
}