import Agenda from 'agenda';
import config from 'config';
import { Db } from 'mongodb';
export default ({ mongoConnection }: { mongoConnection: Db }): Agenda => {
    const { dbCollection, processEvery, maxConcurrency } = config.get('agenda');
    return new Agenda({
        mongo: mongoConnection,
        db: {
            collection: dbCollection, 
        },
        maxConcurrency,
    });
};
