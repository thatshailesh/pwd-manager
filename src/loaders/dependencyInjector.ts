import { Container } from 'typedi';
import LoggerInstance from './logger';
import mongoose from 'mongoose';
import agendaFactory from './agenda';
import { Db } from 'mongodb';
import Agenda from 'agenda';


export default ({ mongoConnection, models }: { mongoConnection: Db; models: { name: string; model: mongoose.Document }[] }): {agenda: Agenda} => {
    try {
        models.forEach(m => {
            Container.set(m.name, m.model);
        });

        const agendaInstance = agendaFactory({ mongoConnection });

        Container.set('agendaInstance', agendaInstance);
        Container.set('logger', LoggerInstance);

        LoggerInstance.info('✌️ loaders injected into container');

        return { agenda: agendaInstance };
    } catch (e) {
        LoggerInstance.error('🔥 Error on dependency injector loader: %o', e);
        throw e;
    }
};
