import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import express from 'express';
import mongooseLoader from './mongoose';
import Logger from './logger';
import jobsLoader from './jobs';

export default async ( expressApp: express.Application): Promise<void> => {
    const mongoConnection = await mongooseLoader();
    Logger.info('✌️ DB loaded and connected!');
    const userModel = {
        name: 'userModel',
        // Notice the require syntax and the '.default'
        model: require('../models/User').default,
    };

    const passwordModel = {
        name: 'passwordModel',
        model: require('../models/Password').default,
    };

    const { agenda } = await dependencyInjectorLoader({
        mongoConnection,
        models: [
            userModel,
            passwordModel,
        ],
    });

    Logger.info('✌️ Dependency Injector loaded');

    await jobsLoader({ agenda });
    Logger.info('✌️ Jobs loaded');

    await new expressLoader(expressApp);
    Logger.info('✌️ Express loaded');
};