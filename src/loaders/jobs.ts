import config from 'config';
import Agenda from 'agenda';
import DeleteRecordJob from '../jobs/deleteRecordJob';

export default async ({ agenda }: { agenda: Agenda }): Promise<void> => {
    const { maxConcurrency } = config.get('agenda');

    agenda.define(
        'soft-delete',

        { priority: 'high', concurrency: maxConcurrency },

        new DeleteRecordJob().handler
    );

    return await agenda.start();
};