import winston from 'winston';
import config from 'config';
import { Container } from 'typedi';
const { level } = config.get('logs');

const transports = [];

if(config.get('NODE_ENV') !== 'dev') 
    transports.push(
        new winston.transports.Console()
    );
else 
    transports.push(
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.cli(),
                winston.format.splat()
            ),
        })
    );
 
    


const LoggerInstance = winston.createLogger({
    level,
    levels: winston.config.npm.levels,
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.json()
    ),
    transports,
});
Container.set('logger', LoggerInstance);
export default LoggerInstance;