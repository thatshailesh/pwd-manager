import mongoose from 'mongoose';
import { Db } from 'mongodb';
import config from 'config';

mongoose.set('useCreateIndex', true);

export default async (): Promise<Db> => {
    const { host } = config.get('mongodb');
    const connection = await mongoose.connect(host, 
        { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false } );
    return connection.connection.db;
};
