import mongoose from 'mongoose';

const { Schema } = mongoose;

const passwordSchema = new Schema(
    {
        accountName: { type: String, required: true },
        password: { type: String, required: true, index: true },
        username: { type: String, required: true },
        userId: {
            type: mongoose.Schema.Types.ObjectId, index: true, ref: 'User',
        },
        isRemoved: { type: Boolean },
    },
    {
        timestamps: { createdAt: 'createdAt' },
        toObject: {
            transform (doc, ret): void {
                delete ret.password;
                delete ret.salt;
                delete ret.__v;
            },
        },
    }
);

passwordSchema.index(
    { 'accountName': 'text', 'username': 'text' }, 
    { 
        'default_language': 'english',
    }    
);
// passwordSchema.index(
//     { 'createdAt': 1 }, 
//     { 
//         'default_language': 'english',
//         'expireAfterSeconds': 120000, // your 2 mins

//     }    
// );
export default mongoose.model('Password', passwordSchema);