import mongoose from 'mongoose';

const { Schema } = mongoose;
const userSchema = new Schema(
    {
        password: { type: String, required: true, index: true },
        username: { type: String, required: true, index: true },
        salt: { type: String },
    },
    {
        toObject: {
            transform (doc, ret): void {
                delete ret.password;
                delete ret.salt;
                delete ret.__v;
            },
        },
    }
);
const userModel = mongoose.model('User', userSchema);

export default userModel;