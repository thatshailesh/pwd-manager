import { Service, Inject } from 'typedi';
import { User as IUser } from '../interfaces/User';
import jwt from 'jsonwebtoken';
import config from 'config';
import argon2 from 'argon2';
import { randomBytes } from 'crypto';
import { AuthService as IAuthService } from '../interfaces/AuthService';
import { BadRequest } from '@curveball/http-errors';
import winston from 'winston';

@Service()
export default class AuthService implements IAuthService {
    @Inject('userModel') private userModel: Models.UserModel;
    @Inject('logger') private logger: winston.Logger;

    public generateToken(user: IUser): string {        
        const secret = config.get('secret') as string;
        const expiry = config.get('expiry') as string;

        this.logger.silly(`Sign JWT for userId: ${user._id}`);

        return jwt.sign(user, secret, { expiresIn: expiry });
    }

    public verifyToken(token: string): any  {
        const secret = config.get('secret') as string;

        return jwt.verify(token, secret);
    }

    public async signUp(user: IUser): Promise<{ user: IUser}> {
        this.logger.silly('Check if user exists already');

        const doesUserExit = await this.userModel.exists({ username: user.username });

        if (doesUserExit) 
            throw new BadRequest('User already have account');

        const salt = randomBytes(32);

        this.logger.silly('Hashing password');

        const hashedPassword = await argon2.hash(user.password, { salt });
        
        this.logger.silly('Creating user db record');

        const userRecord = await this.userModel.create({
            ...user,
            salt: salt.toString('hex'),
            password: hashedPassword,
        });

        this.logger.silly('Generating JWT');

        if (!userRecord) 
            throw new BadRequest('User cannot be created');

        const userData = userRecord.toObject() as IUser;

        return { user: userData };
    }

    public async login(username: string, password: string): Promise<{ user: IUser; token: string }> {
        const userRecord = await this.userModel.findOne({ username });

        if (!userRecord) 
            throw new BadRequest('User not registered');

        this.logger.silly('Checking password');

        const validPassword = await argon2.verify(userRecord.password, password);

        if (validPassword) {
            this.logger.silly('Password is valid!');
            this.logger.silly('Generating JWT');

            const user = userRecord.toObject();

            const token = this.generateToken(user);

            return { user, token };
        } else {
            throw new BadRequest('Invalid Credentials');
        }
    }
}