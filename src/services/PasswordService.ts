
import { PasswordService as IPasswordService } from '../interfaces/PasswordService';
import { Service, Inject, Container } from 'typedi';
import { PasswordData } from '../interfaces/PasswordData';
import winston = require('winston');
import { Forbidden, BadRequest, NotFound } from '@curveball/http-errors/dist';
import { randomBytes } from 'crypto';
import argon2 from 'argon2';
import { PaginOptions } from '../interfaces/PagingOptions';
import Agenda = require('agenda');
import { PaginatedPasswordData } from '../interfaces/PaginatedPasswordData';
import config from 'config';
@Service()
export default class PasswordService implements IPasswordService {
    @Inject('passwordModel') private passwordModel: Models.PasswordModel
    @Inject('logger') private logger: winston.Logger;

    private mapping: any = {
        ascending: 1,
        descending: -1,
    }

    public async createData(data: PasswordData): Promise<{passwordData: PasswordData}> {
        this.logger.silly('Create new password data');

        const doesPasswordDataExists = await this.passwordModel.exists(
            {
                userId: data.userId,
                username: data.username,
            }
        );
        if (doesPasswordDataExists) 
            throw new Forbidden('Password data already exists');

        const salt = randomBytes(32);

        const hashedPassword = await argon2.hash(data.password, { salt });

        this.logger.silly('Creating db record');

        const passwordRecord = await this.passwordModel.create({
            ...data,
            salt: salt.toString('hex'),
            password: hashedPassword,
        });

        if (!passwordRecord) 
            throw new BadRequest('Password data cannot be created');

        const passwordData = passwordRecord.toObject() as PasswordData;

        this.logger.silly('Db record created successfully');
        return { passwordData };
    }

    public async updateOne(_id: string, data: PasswordData): Promise<{passwordData: PasswordData}> {
        this.logger.silly('Update password data');

        const doesPasswordDataExists = await this.passwordModel.exists({ _id, userId: data.userId });

        if (!doesPasswordDataExists) 
            throw new NotFound(`Password data not found for id ${_id}`);

        this.logger.silly('Updating db record');

        if (data.isRemoved) {
            this.logger.silly('Schedule Soft delete job');
            await this.scheduleDeleteJob(_id);
            
            this.logger.silly('Soft delete job scheduled');

        }

        let passwordRecord;

        if (data.password) {
            const salt = randomBytes(32);

            const hashedPassword = await argon2.hash(data.password, { salt });
        
            this.logger.silly('Creating db record');

            passwordRecord = await this.passwordModel.findOneAndUpdate(
                { _id },
                {
                    ...data,
                    salt: salt.toString('hex'),
                    password: hashedPassword,
                },
                { new: true }
            );
        }else {
            passwordRecord = await this.passwordModel.findOneAndUpdate(
                { _id },
                {
                    ...data,
                },
                { new: true }
            );
        }

        if (!passwordRecord) 
            throw new BadRequest('Password data cannot be created');
        
        this.logger.silly('Db record updated successfully');
        
        return { passwordData: passwordRecord };
    }

    public async findOne(_id: string): Promise<{passwordData: PasswordData}> {
        this.logger.silly('find password data');

        const passwordData = await this.passwordModel.findOne({ _id });
        if (!passwordData) 
            throw new NotFound('Password Data not found');

        return { passwordData };
    }

    public async findAll(userId: string, paging: PaginOptions): Promise<PaginatedPasswordData> {
        if (!userId) throw new BadRequest('User id not found');
        let query: any = { };

        if (paging.criteria) 
            query = {
                userId,
                $text: {
                    $search: paging.criteria,
                },
            };
        
        this.logger.silly('Find all password data');

        const { recordsPerPage = 0, pageNumber = 0, sort = -1 }  = paging;
        if (paging.sort)
            paging.sort = this.mapping[paging.sort];

        const passwordData = await this.passwordModel
            .find(query)
            .sort({ _id: sort })
            .skip(pageNumber * recordsPerPage)
            .limit(recordsPerPage);

        return { pageNumber, recordsPerPage, data: passwordData };
    }

    public async deleteOne(_id: string): Promise<{deleted: boolean}> {
        this.logger.silly('delete password data');
        const doesPasswordDataExists = await this.passwordModel.exists({ _id });

        if (!doesPasswordDataExists) {
            this.logger.silly('Data not found');
            throw new NotFound('Password data not found');
        }

        const result = await this.passwordModel.deleteOne({ _id });

        if (result && result.deletedCount) 
            return { deleted: true };

        return { deleted: false };
    }

    private async scheduleDeleteJob(id: string): Promise<Agenda.Job> {
        const agendaInstance = Container.get('agendaInstance') as Agenda;
        const { schedulesIn } = config.get('agenda');
        return await agendaInstance.schedule(schedulesIn, 'soft-delete', { _id: id });
    }
}