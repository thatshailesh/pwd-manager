import { Document, Model } from 'mongoose';
import { User as IUser } from '../../interfaces/User';
import { PasswordData as IPassword } from '../../interfaces/PasswordData';
declare global {
  namespace Express {
    export interface Request {
      currentUser: IUser & Document;
    }    
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type PasswordModel = Mode<IPassword & Document>;
  }
}
